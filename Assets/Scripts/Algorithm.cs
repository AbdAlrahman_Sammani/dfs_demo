﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using UnityEngine;
using System.Threading;
using UnityEngine.UI;

public class Algorithm : MonoBehaviour
{
    private bool isAlgorithmRun = false;
    public AppProcess appProcess;
    public int[] goal;
    public int[] target;
    [System.Serializable]

    public struct Mines
    {
        public int xpos { get; set; }
        public int ypos { get; set; }
    }
    [SerializeField]
    public List<Mines> minesList;
    class Node
    {

        public Node(Point v, int lvl)
        {
            value = v;
            level = lvl;
        }
        public Node()
        {

        }
        public Point value = new Point();
        public int level;
        public Queue<Point> parent1 = new Queue<Point>();

    }
    // Start is called before the first frame update
    void Start()
    {
        //Thread thread = new Thread(DFSALgotithm);
        //thread.Start();
        //  StartCoroutine(DFSALgotithm());
    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator DFSALgotithm()
    {
        Debug.Log("Started");
        Point outputpoint = new Point();
        List<Point> Mines = new List<Point>();
        //foreach (var item in minesList)
        //{
        //    Mines.Add(new Point { X = item.xpos, Y = item.ypos });
        //    appProcess.rows[item.xpos].rowObject[item.ypos].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.black;

        //}
        Mines.Add(new Point { X = M1xStartPs, Y = M1ystartps });
        appProcess.rows[M1xStartPs].rowObject[M1ystartps].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.black;
        Mines.Add(new Point { X = M2xStartPs, Y = M2ystartps });
        appProcess.rows[M2xStartPs].rowObject[M2ystartps].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.black;
        Mines.Add(new Point { X = M3xStartPs, Y = M3ystartps });
        appProcess.rows[M3xStartPs].rowObject[M3ystartps].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.black;

       // Mines.Add(new Point { X = 4, Y = 8 });
        //Mines.Add(new Point { X = 7, Y = 2 });
      //  LinkedList<Node>[] pa = new LinkedList<Node>[8];
        Stack<Node> expanded = new Stack<Node>();
        LinkedList<Point> path1 = new LinkedList<Point>();
        string[] tstdata = System.IO.File.ReadAllLines(Application.streamingAssetsPath + "/Input.txt");
        for (int j = 0; j < tstdata.Length; j++)
        {
            expanded.Clear();
            int x1 = xStartPs;//Convert.ToInt32(tstdata[j].Split(' ')[0].Split(',')[0]);
            int y1 = ystartps; //Convert.ToInt32(tstdata[j].Split(' ')[0].Split(',')[1]);
            appProcess.rows[x1].rowObject[y1].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.blue;

            expanded.Push(new Node(new Point(x1, y1), 0));
            Point child = new Point();
            int lvlnode = 1;
            outputpoint.X = x_endPos; //Convert.ToInt32(Convert.ToInt32(tstdata[j].Split(' ')[1].Split(',')[0]));
            outputpoint.Y = y_endPos; //Convert.ToInt32(Convert.ToInt32(tstdata[j].Split(' ')[1].Split(',')[1]));
            appProcess.rows[outputpoint.X].rowObject[outputpoint.Y].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.yellow;

            while (true)
            {
                Debug.Log("Processing....");
                Node next1 = new Node();
                next1 = expanded.Pop();
                next1.parent1.Enqueue(next1.value);
                child.X = next1.value.X + 1;
                child.Y = next1.value.Y;
                if (!(child.X < 0 || child.X > 9 || child.Y > 9 || child.Y < 0))
                {

                    bool flag2 = true;
                    while (flag2)
                    {
                        flag2 = false;
                        foreach (var mi in Mines)
                        {
                            if (mi.X == child.X)
                            {
                                child.X += 1;
                                flag2 = true;
                                break;
                            }
                        }
                    }
                    Node ne = new Node();
                    int flag = 0;
                    foreach (Point x in next1.parent1)
                    {
                        if (x == child)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {
                        foreach (Point p in next1.parent1)
                        {
                            ne.parent1.Enqueue(p);
                        }
                        ne.value = child;
                        ne.level = lvlnode;
                        expanded.Push(ne);
                    }
                    if (child == outputpoint)
                    {
                        using (StreamWriter events = new StreamWriter(Application.streamingAssetsPath + "/output.txt", true))
                        {
                            events.WriteLine("Goal {0} : Target {1}", tstdata[j].Split(' ')[0], tstdata[j].Split(' ')[1]);
                            // appProcess.rows[int.Parse(tstdata[j].Split(' ')[0])].rowObject[int.Parse(tstdata[j].Split(' ')[1])].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.blu;

                            events.WriteLine("Path to Target");
                            foreach (Point x in ne.parent1)
                            {
                                appProcess.rows[x.X].rowObject[x.Y].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.red;
                                yield return new WaitForSeconds(2);
                                events.WriteLine("(" + x.X.ToString() + "," + x.Y.ToString() + ")");
                            }
                            events.WriteLine("(" + tstdata[j].Split(' ')[1] + ")");
                            events.WriteLine("-----------------------------------------");
                        }
                        break;
                    }
                }

                child.X = next1.value.X - 1;
                child.Y = next1.value.Y;
                if (!(child.X < 0 || child.X > 9 || child.Y > 9 || child.Y < 0))
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        flag2 = false;
                        foreach (var mi in Mines)
                        {
                            if (mi.X == child.X)
                            {
                                child.X -= 1;

                                flag2 = true;
                                break;
                            }
                        }
                    }
                    Node ne = new Node();
                    int flag = 0;
                    foreach (Point x in next1.parent1)
                    {
                        if (x == child)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {
                        foreach (Point p in next1.parent1)
                        {
                            ne.parent1.Enqueue(p);
                        }
                        ne.value = child;
                        ne.level = lvlnode;
                        expanded.Push(ne);
                    }
                    if (child == outputpoint)
                    {
                        using (StreamWriter events = new StreamWriter(Application.streamingAssetsPath + "output.txt", true))
                        {
                            events.WriteLine("Goal {0} : Target {1}", tstdata[j].Split(' ')[0], tstdata[j].Split(' ')[1]);
                            events.WriteLine("Path to Target");
                            foreach (Point x in ne.parent1)
                            {
                                appProcess.rows[x.X].rowObject[x.Y].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.red;
                                yield return new WaitForSeconds(2);
                                events.WriteLine("(" + x.X.ToString() + "," + x.Y.ToString() + ")");
                            }
                            events.WriteLine("(" + tstdata[j].Split(' ')[1] + ")");
                            events.WriteLine("-----------------------------------------");
                        }
                        break;
                    }
                }

                child.X = next1.value.X;
                child.Y = next1.value.Y + 1;
                if (!(child.X < 0 || child.X > 9 || child.Y > 9 || child.Y < 0))
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        flag2 = false;
                        foreach (var mi in Mines)
                        {
                            if (mi.Y == child.Y)
                            {
                                child.Y += 1;

                                flag2 = true;
                                break;
                            }

                        }
                    }
                    Node ne = new Node();
                    int flag = 0;
                    foreach (Point x in next1.parent1)
                    {
                        if (x == child)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {

                        foreach (Point p in next1.parent1)
                        {
                            ne.parent1.Enqueue(p);
                        }
                        ne.value = child;
                        ne.level = lvlnode;
                        expanded.Push(ne);
                    }
                    if (child == outputpoint)
                    {
                        using (StreamWriter events = new StreamWriter(Application.streamingAssetsPath + "/output.txt", true))
                        {
                            events.WriteLine("Goal {0} : Target {1}", tstdata[j].Split(' ')[0], tstdata[j].Split(' ')[1]);

                            string firstNum = tstdata[j].Split(' ')[0].Substring(0, 1);
                            string secondNum = tstdata[j].Split(' ')[0].Substring(2);
                            Debug.Log(firstNum);
                            Debug.Log(secondNum);

                            events.WriteLine("Path to Target");
                            foreach (Point x in ne.parent1)
                            {
                                appProcess.rows[x.X].rowObject[x.Y].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.red;
                                yield return new WaitForSeconds(2);
                                events.WriteLine("(" + x.X.ToString() + "," + x.Y.ToString() + ")");
                            }
                            events.WriteLine("(" + tstdata[j].Split(' ')[1] + ")");
                            events.WriteLine("-----------------------------------------");
                        }
                        break;
                    }

                }

                child.X = next1.value.X;
                child.Y = next1.value.Y - 1;
                if (!(child.X < 0 || child.X > 9 || child.Y > 9 || child.Y < 0))
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        flag2 = false;
                        foreach (var mi in Mines)
                        {
                            if (mi.Y == child.Y)
                            {
                                child.Y -= 1;
                                flag2 = true;
                                break;
                            }

                        }
                    }
                    Node ne = new Node();
                    int flag = 0;
                    foreach (Point x in next1.parent1)
                    {
                        if (x == child)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {
                        foreach (Point p in next1.parent1)
                        {
                            ne.parent1.Enqueue(p);
                        }
                        ne.value = child;
                        ne.level = lvlnode;
                        expanded.Push(ne);
                    }
                    if (child == outputpoint)
                    {
                        using (StreamWriter events = new StreamWriter(Application.streamingAssetsPath + "output.txt", true))
                        {
                            events.WriteLine("Goal {0} : Target {1}", tstdata[j].Split(' ')[0], tstdata[j].Split(' ')[1]);
                            events.WriteLine("Path to Target");
                            foreach (Point x in ne.parent1)
                            {
                                Debug.Log(x.X);

                                appProcess.rows[x.X].rowObject[x.Y].GetComponent<MeshRenderer>().material.color = UnityEngine.Color.red;
                                yield return new WaitForSeconds(2);
                                events.WriteLine("(" + x.X.ToString() + "," + x.Y.ToString() + ")");
                            }
                            events.WriteLine("(" + tstdata[j].Split(' ')[1] + ")");
                            events.WriteLine("-----------------------------------------");
                        }
                        break;
                    }

                }
                lvlnode++;
                yield return null;
            }
            yield return null;
        }
        // Console.WriteLine("done");
        Debug.Log("Done");
        noSolutionText.text = "Done!";

    }
    int xStartPs, ystartps, x_endPos, y_endPos;
    int M1xStartPs, M1ystartps, M2xStartPs, M2ystartps, M3xStartPs, M3ystartps;
    public Text noSolutionText;

    public void StartAlgorithm()
    {
        if (!isAlgorithmRun)
        {
            xStartPs = int.Parse(appProcess.xStartPos.text);
            ystartps = int.Parse(appProcess.yStartPos.text);

            x_endPos = int.Parse(appProcess.xEndPos.text);
            y_endPos = int.Parse(appProcess.yEndPos.text);

            M1xStartPs = int.Parse(appProcess.mine1_xPos.text);
            M1ystartps = int.Parse(appProcess.mine1YPos.text);

            M2xStartPs = int.Parse(appProcess.mine2_xPos.text);
            M2ystartps = int.Parse(appProcess.mine2YPos.text);

            M3xStartPs = int.Parse(appProcess.mine3_xPos.text);
            M3ystartps = int.Parse(appProcess.mine3YPos.text);

            if (xStartPs == M1xStartPs || ystartps == M1ystartps || xStartPs == M2xStartPs || ystartps == M2ystartps || xStartPs == M2xStartPs || ystartps == M2ystartps)
            {
                Debug.Log("No Solution");
                noSolutionText.text = "No Solution";
            }
            else if (x_endPos == M1xStartPs || y_endPos == M1ystartps || x_endPos == M2xStartPs || y_endPos == M2ystartps || x_endPos == M2xStartPs || y_endPos == M2ystartps)
            {
                Debug.Log("No Solution");
                noSolutionText.text = "No Solution";
            }
            else
            {
                StartCoroutine(DFSALgotithm());
                noSolutionText.text = "Processing";

                isAlgorithmRun = true;
                FindObjectOfType<Button>().GetComponentInChildren<Text>().text = "Restart";

            }
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}


