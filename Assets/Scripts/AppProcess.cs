﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AppProcess : MonoBehaviour
{
    public InputField xStartPos;
    public InputField yStartPos;

    public InputField xEndPos;
    public InputField yEndPos;

    public InputField mine1_xPos, mine1YPos, mine2_xPos, mine2YPos, mine3_xPos, mine3YPos;
    [System.Serializable]
    public struct Rows
    {
        public List<GameObject> rowObject;
    }
    public List<Rows> rows;
  

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
